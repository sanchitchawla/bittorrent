import socket
from threading import Thread
import time
import struct
import socket
from SimpleHTTPServer import SimpleHTTPRequestHandler
import os.path
from urllib2 import urlopen
import shutil
import sys
import os
import SocketServer


command = sys.argv
Filename = command[1]
order = command[2]

trackport = 60000
PORT = 14343



def get_ip_address():
    s = socket.socket(socket.AF_INET,socket.AF_INET)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


my_ip = get_ip_address()
broadcastlist = my_ip.split(".")
broadcastlist.pop(-1)
broadcastip = '.'.join(map(str, broadcastlist))


def broadcast():

    cs = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    print "Broadcast begin"
    print "My ip is", my_ip
    while 1:
        for i in range (0,255):
            data = "PAM Grab the file at http://{}:{}/{}".format(my_ip,"8080",Filename)
            cs.sendto(data, (broadcastip+"."+str(i), PORT))
    time.sleep(5)

def getURL():
    s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((my_ip,PORT))
    while 1:
        message, addr = s.recvfrom(1024)
        if "PAM" in message and addr[0] != my_ip:
            print "Discovered" , addr
            url = message.split(" ")[-1]
            return url
    time.sleep(1)

def HttpServer():
    Handler = SimpleHTTPRequestHandler

    httpd = SocketServer.TCPServer((my_ip, 8080), Handler)

    # print "serving at port" + "ip", PORT, my_ip
    httpd.serve_forever()
    time.sleep(1)

def isFile(fname):
    return os.path.isfile(fname)


def client():
    while True:
        url = getURL()
        print "Download from" , url
        try:
            response = urlopen(url)
            file_name = url.split("/")[-1]
            CHUNK = 64 * 1024
            with open(file_name, 'wb') as f:
                while True:
                    chunk = response.read(CHUNK)
                    if not chunk:
                        break
                    f.write(chunk)
                f.close()

            print "Finish Downloading"
            print "Start Seeding"
        except Exception:
            pass
        time.sleep(5)
# def tracker():
#     s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     s.bind((my_ip,trackport))
#     while 1:
#         message,addr=s.recvfrom(1024)
#         print message + "from" + addr
#         time.sleep(1)
#
# def progressbroadcast():
#     cs = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
#     cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#     cs.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
#      #get our IP. Be careful if you have multiple network interfaces or IPs
#
#     while 1:
#         data = "Download Finished" + my_ip
#         cs.sendto(data, (my_ip, trackport))
#         time.sleep(3)

if __name__ == '__main__':

    if order == "seed":

        HttpServ = Thread(target=HttpServer)
        HttpServ.daemon = True
        HttpServ.start()

        Broadcast = Thread(target=broadcast)
        Broadcast.daemon = True # this is to make CTRL-C works
        Broadcast.start()

        # track = Thread(target=tracker)
        # track.daemon = True
        # track.start()

        while True:
            time.sleep(1)
    elif order == "download":
        cl = Thread(target=client)
        cl.daemon = True
        cl.start()
        if isFile(Filename):
            print "Shutting down Thread"
            cl.stop()
            HttpServ = Thread(target=HttpServer)
            HttpServ.daemon = True
            HttpServ.start()

            Broadcast = Thread(target=broadcast)
            Broadcast.daemon = True  # this is to make CTRL-C works
            Broadcast.start()

            # progressbc = Thread(target=progressbroadcast)
            # progressbc.daemon = True
            # progressbc.start()
            while True:
                time.sleep(1)
        # geturl = Thread(target=getURL)
        # geturl.daemon = True
        # geturl.start()
        while True:
            time.sleep(1)
    elif order == "discover":
        dc = Thread(target=getURL)
        dc.daemon = True
        dc.start()
        Broadcast = Thread(target=broadcast)
        Broadcast.daemon = True  # this is to make CTRL-C works
        Broadcast.start()
        while True:
            time.sleep(1)


