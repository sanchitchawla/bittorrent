from threading import Thread
import time
import socket
from SimpleHTTPServer import SimpleHTTPRequestHandler
import os.path
from urllib2 import urlopen
import sys
import os
import SocketServer



command = sys.argv
Filename = command[1]
trackport = 4400
port = 6969
tcp_port = 9596

def get_broadcast_ip():
    broadcastlist = get_ip_address().split(".")
    broadcastlist.pop(-1)
    broadcastip = '.'.join(map(str, broadcastlist))
    return broadcastip

def get_ip_address():
    s = socket.socket(socket.AF_INET,socket.AF_INET)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()
    return local_ip


def broadcast():
    cs = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    print "Broadcast started"
    while 1:
        for i in range (0,256):
            data = "http://{}:{}/{} ".format(get_ip_address(),"8080",Filename)
            # print get_broadcast_ip()+"."+str(i), port
            cs.sendto(data, (get_broadcast_ip() +"." +str(i), port))

    time.sleep(15)


def httpserver_exec():
    print "Start seeding"
    Handler = SimpleHTTPRequestHandler
    httpd = SocketServer.TCPServer((get_ip_address(), 8080), Handler)

    # print "serving at port" + "ip", PORT, my_ip
    httpd.serve_forever()
    time.sleep(1)

def is_file(fname):
    return os.path.isfile(fname)

def broadcast_tracker():
    cs = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print "Broadcast begin"
    while 1:
        for i in range (0,256):
            data = "http://{}:{}/{} ".format(get_ip_address(),"8080",Filename)
            cs.sendto(data, (get_broadcast_ip()+"."+str(i), trackport))

    time.sleep(5)

def get_tracker_ip():
    print "Finding tracker ip"
    s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.bind((get_ip_address(),trackport))

    while 1:
        message, addr = s.recvfrom(1024)
        # if "TRACK" in message and addr[0] != get_ip_address():
        #     print "Discovered" , addr
        url = message.split(" ")[0]
        return url
    time.sleep(1)

def get_seed_ip():
    print "Finding seeder"
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((get_ip_address(), port))
    while 1:
        message = s.recv(1024)
        # if "PAM" in message and addr[0] != get_ip_address():
        url = message.split(" ")[0]
        print url
        s.close()
        return url
    time.sleep(1)


def download():
    while True:
        try:
            "Finding"
            url = get_seed_ip()
            f = urlopen(url)
            if f.getcode() == 200:
                break
        except:
            pass

    # while True:
    #     url = get_seed_ip()
    #     print url
    #
    #     f = urlopen(url)
    #     code = f.getcode()
    #     file_name = url.split("/")[-1]
    #
    #     if code == 200:
    #         break
    #     else:continue

    file_name = url.split("/")[-1]
    print "Starting Download"
    content_length = float(f.headers['content-length'])
    with open(file_name, 'wb',20000000) as file:
        file_size_dl = 0
        block = 20000000
        while True:
            buffer = f.read(block)
            # print len(buffer)
            if not buffer:
                print "Download finish"
                file.close()
                return True
                break
            file.write(buffer)
            file_size_dl += len(buffer)
            percentDL = percentDownload(file_size_dl, content_length)

            print '{}%'.format(percentDL)

#
# def send_track_data(current_byte):
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     tracker_ip = get_tracker_ip()
#     s.connect((tracker_ip, tcp_port))
#     s.send(current_byte)
#     s.close()

def percentDownload(curr, full):
    return curr*100./full

class tracker_server(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(60)
            Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):
        size = 1024
        ip = address
        while True:
            try:
                data = client.recv(size)
                if data:
                    print data
                else:
                    raise ('Client disconnected')
            except:
                client.close()
                return False


if __name__ == "__main__":
        if command[-1] == "Track":
            print "Tracker Online"
            tracker_server(get_ip_address(),tcp_port)



        if is_file(Filename):
            http_thread = Thread(target=httpserver_exec)
            http_thread.daemon = True
            http_thread.start()

            broadcast_thread = Thread(target=broadcast)
            broadcast_thread.daemon = True
            broadcast_thread.start()

            while True:
                time.sleep(1)

        else:
            print "File not found"
            seed_discover = Thread(target=get_seed_ip)
            seed_discover.daemon = True
            seed_discover.start()



            tracker_discover = Thread(target=get_tracker_ip)
            tracker_discover.daemon = True
            tracker_discover.start()
            if download():
                http_thread = Thread(target=httpserver_exec)
                http_thread.daemon = True
                http_thread.start()

                broadcast_thread = Thread(target=broadcast)
                broadcast_thread.daemon = True
                broadcast_thread.start()

                while True:
                    time.sleep(1)

            while True:
                time.sleep(1)


